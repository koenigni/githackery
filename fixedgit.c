
#include <asm/unistd.h>
#include <asm/unistd_64.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/wait.h>
#include <unistd.h>

#include <vector>

pid_t
launch_git(int argc, char **argv) {
  pid_t child;

  if ((child = fork()) == 0) {
	ptrace(PTRACE_TRACEME, 0, 0, 0);
	execvp("git", argv);
  } else {
	int status;
	waitpid(child, &status, 0);
	ptrace(PTRACE_SETOPTIONS, child, 0,
		   PTRACE_O_TRACESYSGOOD | PTRACE_O_TRACECLONE | PTRACE_O_TRACEEXIT);
  }
  return child;
}

int
adjust_syscall(pid_t child) {
  struct user_regs_struct r;
  ptrace(PTRACE_GETREGS, child, NULL, &r);
  dprintf(2, "Intercepted %llu\n", r.orig_rax);
  switch (r.orig_rax) {
	case __NR_newfstatat:
	  r.r10 &= ~((unsigned long) AT_SYMLINK_NOFOLLOW);
	  // printf("%llx %llx %llx %llx\n", r.rdi, r.rsi, r.rdx, r.r10);
	  ptrace(PTRACE_SETREGS, child, NULL, &r);
	  break;
	default:
	  break;
  }
  return 1;
}


void
register_new_pid(pid_t spawner, std::vector<pid_t> &pids) {
  unsigned long msg;
  ptrace(PTRACE_GETEVENTMSG, spawner, NULL, &msg);
  dprintf(2, "=================== New process: %lu\n", msg);
  pids.push_back((pid_t) msg);
}

void
handle_next_ptrace_event(pid_t *childr, std::vector<pid_t> &pids) {
  int status;
  siginfo_t siginfo;
  pid_t child = *childr;

  waitpid(child, &status, 0);

  ptrace(PTRACE_GETSIGINFO, child, 0, &siginfo);

  int is_traceclone_stop = status >> 8 == (SIGTRAP | (PTRACE_EVENT_CLONE << 8));
  int is_exit_stop = status >> 8 == (SIGTRAP | (PTRACE_EVENT_EXIT << 8));
  int is_syscall_stop = WIFSTOPPED(status) && (WSTOPSIG(status) & 0x80);

  dprintf(2, "Handling event %#x (%d %d %d)\n", status, is_traceclone_stop,
		  is_syscall_stop, is_exit_stop);

  if (is_exit_stop) {
	*childr = (pid_t) -1;
	dprintf(2, "Exited: %d with status %d\n", child, status);
	return;
  }

  if (is_traceclone_stop) {
	register_new_pid(child, pids);
	ptrace(PTRACE_SYSCALL, child, 0, 0);
	return;
  }

  if (is_syscall_stop) {
	adjust_syscall(child);
	ptrace(PTRACE_SYSCALL, child, 0, 0);
	return;
  }

  ptrace(PTRACE_SYSCALL, child, 0, siginfo.si_signo);
}

void
trace(pid_t child) {
  ptrace(PTRACE_SYSCALL, child, 0, 0);
  std::vector<pid_t> pids;
  pids.push_back(child);
  unsigned int cons_empty = 0;
  int i = 0;
  while (true) {
	if (pids[i] == (pid_t) -1) {
	  cons_empty++;
	  if (cons_empty > pids.size())
		return;
	} else {
	  cons_empty = 0;

	  if (pids.size() == 1) {
		dprintf(2, "PIDS: %zu: %d (curr %d)\n", pids.size(), pids[0], pids[i]);
	  } else if (pids.size() == 2) {
		dprintf(2, "PIDS: %zu: %d %d (curr %d)\n", pids.size(), pids[0],
				pids[1], pids[i]);
	  }

	  handle_next_ptrace_event(&pids[i], pids);
	}

	i = (i + 1) % pids.size();
  }
}

int
main(int argc, char **argv) {
  pid_t child = launch_git(argc, argv);

  trace(child);

  return 0;
}
